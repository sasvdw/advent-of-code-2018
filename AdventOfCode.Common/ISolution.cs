﻿using System;

namespace AdventOfCode.Common
{
    public interface ISolution<TInput, TOutputOne, TOutputTwo>
    {
        TOutputOne RunPartOne(in Span<TInput> input);
        TOutputTwo RunPartTwo(in Span<TInput> input);
    }
}
