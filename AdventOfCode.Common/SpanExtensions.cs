using System;

namespace AdventOfCode.Common
{
    public static class SpanExtensions
    {
        public delegate TProperty Selector<in TInput, out TProperty>(TInput x) where TProperty : IComparable<TProperty>;

        public static void SortBy<TInput, TProperty>(this Span<TInput> span, Selector<TInput, TProperty> propertySelector)
            where TProperty : IComparable<TProperty>
        {
            span.QuickSort(0, span.Length - 1, propertySelector);
        }

        private static void QuickSort<TInput, TProperty>(this Span<TInput> span, int left, int right, Selector<TInput, TProperty> propertySelector)
            where TProperty : IComparable<TProperty>
        {
            if(left > right || left < 0 || right < 0) return;

            var index = span.Partition(left, right, propertySelector);

            if(index != -1)
            {
                span.QuickSort(left, index - 1, propertySelector);
                span.QuickSort(index + 1, right, propertySelector);
            }
        }

        private static int Partition<TInput, TProperty>(this Span<TInput> span, int left, int right, Selector<TInput, TProperty> propertySelector)
            where TProperty : IComparable<TProperty>

        {
            if(left > right) return -1;

            var end = left;

            var pivot = span[right]; // choose last one to pivot, easy to code
            for(int i = left; i < right; i++)
            {
                var x = propertySelector(pivot);
                var y = propertySelector(span[i]);
                var result = x.CompareTo(y);
                if(result > 0)
                {
                    Swap(span, i, end);
                    end++;
                }
            }

            Swap(span, end, right);

            return end;
        }

        private static void Swap<T>(Span<T> span, int left, int right)
        {
            var tmp = span[left];
            span[left] = span[right];
            span[right] = tmp;
        }
    }
}
