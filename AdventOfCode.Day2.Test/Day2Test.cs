using AdventOfCode.Common;
using Common;
using Day2;

namespace AdventOfCode.Day2.Test
{
    public class Day2Test : TestRunnerBase<string, int, string>
    {
        public Day2Test() : base(new SolutionDay2()) { }

        protected override string[] InputOne =>
            new[]
            {
                "abcdef",
                "bababc",
                "abbcde",
                "abcccd",
                "aabcdd",
                "abcdee",
                "ababab",
            };

        protected override string[] InputTwo =>
            new[]
            {
                "abcde",
                "fghij",
                "klmno",
                "pqrst",
                "fguij",
                "axcye",
                "wvxyz",
            };

        protected override int ExpectedOne => 12;
        protected override string ExpectedTwo => "fgij";
    }
}
