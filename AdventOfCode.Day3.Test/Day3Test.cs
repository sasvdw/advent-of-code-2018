using System;
using Common;
using Xunit;

namespace AdventOfCode.Day3.Test
{
    public class Day3Test : TestRunnerBase<string, int, int>
    {
        private readonly string[] input =
            new[]
            {
                "#1 @ 1,3: 4x4",
                "#2 @ 3,1: 4x4",
                "#3 @ 5,5: 2x2"
            };

        public Day3Test() : base(new SolutionDay3(8)) { }

        protected override string[] InputOne => this.input;
        protected override string[] InputTwo => this.input;

        protected override int ExpectedOne => 4;
        protected override int ExpectedTwo => 3;
    }
}
