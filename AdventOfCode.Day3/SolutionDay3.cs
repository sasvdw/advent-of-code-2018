using System;
using System.Text.RegularExpressions;
using AdventOfCode.Common;
using static System.Math;

namespace AdventOfCode.Day3
{
    public class SolutionDay3 : ISolution<string, int, int>
    {
        private readonly Regex regex = new Regex(@"#(?'ID'[\d]+) @ (?'xPos'[\d]+),(?'yPos'[\d]+): (?'width'[\d]+)x(?'height'[\d]+)", RegexOptions.Compiled);

        private readonly int size;

        public SolutionDay3(int size)
        {
            this.size = size;
        }

        public int RunPartOne(in Span<string> input)
        {
            var size = this.size;
            var claims = this.ReadClaims(input);

            Span<ushort> squares = new ushort[size * size];
            foreach(var claim in claims)
            {
                for(int x = claim.PosX; x < claim.PosX + claim.Width; x++)
                {
                    for(int y = claim.PosY; y < claim.PosY + claim.Height; y++)
                    {
                        var index = x + (size * y);
                        squares[index]++;
                    }
                }
            }

            var count = 0;

            for(int i = 0; i < squares.Length; i++)
            {
                count += Max(Min((ushort)2, squares[i]) - 1, 0);
            }

            return count;
        }

        private Span<AABB> ReadClaims(in Span<string> input)
        {
            Span<AABB> claims = new AABB[input.Length];

            for(var i = 0; i < input.Length; i++)
            {
                claims[i] = this.ReadClaim(input[i]);
            }

            return claims;
        }

        public int RunPartTwo(in Span<string> input)
        {
            var claims = this.ReadClaims(input);
            Span<short> overlaps = new short[input.Length];

            for(int i = 0; i < claims.Length - 1; i++)
            {
                for(int j = i + 1; j < claims.Length; j++)
                {
                    if(claims[i].CheckOverlaps(claims[j]))
                    {
                        overlaps[i]++;
                        overlaps[j]++;
                    }
                }
            }

            for(int i = 0; i < overlaps.Length; i++)
            {
                if(overlaps[i] == 0)
                {
                    return claims[i].Id;
                }
            }

            return 0;
        }

        private AABB ReadClaim(string claim)
        {
            var match = this.regex.Match(claim);

            var id = int.Parse(match.Groups["ID"].Value);
            var posX = int.Parse(match.Groups["xPos"].Value);
            var posY = int.Parse(match.Groups["yPos"].Value);
            var width = int.Parse(match.Groups["width"].Value);
            var height = int.Parse(match.Groups["height"].Value);

            return (id, posX, posY, width, height);
        }

        private struct AABB
        {
            public readonly int Id;
            public readonly int PosX;
            public readonly int PosY;
            public readonly int Width;
            public readonly int Height;

            public AABB(int id, int posX, int posY, int width, int height)
            {
                this.Id = id;
                this.PosX = posX;
                this.PosY = posY;
                this.Width = width;
                this.Height = height;
            }

            public static implicit operator AABB((int id, int posX, int posY, int width, int height) val)
            {
                return new AABB(val.id, val.posX, val.posY, val.width, val.height);
            }

            public bool CheckOverlaps(AABB other)
            {
                return this.PosX < other.PosX + other.Width &&
                       this.PosX + this.Width > other.PosX &&
                       this.PosY < other.PosY + other.Height &&
                       this.PosY + this.Height > other.PosY;
            }
        }
    }
}
