﻿using System;
using static Input.FileInputReader;

namespace AdventOfCode.Day4
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = ReadInputFile("input.txt");
        
            var solution = new SolutionDay4();

            Console.WriteLine($"Part 1: {solution.RunPartOne(input)}");
            Console.WriteLine($"Part 2: {solution.RunPartTwo(input)}");
        }
    }
}
