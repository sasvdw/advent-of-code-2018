﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using AdventOfCode.Common;
using static System.Math;

namespace AdventOfCode.Day4
{
    public class SolutionDay4 : ISolution<string, int, int>
    {
        private const ulong allOnes = ~0ul;
        private const int ulongSize = sizeof(ulong) * 8;
        
        private readonly Regex regex = new Regex(@"\[(?'Year'\d{4})-(?'Month'\d{2})-(?'Day'\d{2}) (?'Hour'\d{2}):(?'Minute'\d{2})\] ((?'WakesUp'wakes up)|(?'FallsAsleep'falls asleep)|(?'GuardEnters'Guard #(?'GuardID'[\d]+) begins shift))", RegexOptions.Compiled);
        
        public int RunPartOne(in Span<string> input)
        {
            var entries = this.ReadEntries(input);

            entries.SortBy(x => x.Timestamp);

            var dayMin = entries[0].Timestamp.DayOfYear;
            var dayMax = entries[entries.Length - 1].Timestamp.DayOfYear;
            var daysCount = dayMax - dayMin + 1;
            Span<ulong> sleepMasks = new ulong[daysCount];
            Span<short> guardOnDuties = new short[daysCount];
            Span<short> timeAsleep = new short[daysCount];

            short guardOnDuty = 0;
            short asleepMinute = 0;
            for(var i = 0; i < entries.Length; i++)
            {
                var entry = entries[i];
                if(entry.Action == GuardAction.Enters)
                {
                    guardOnDuty = entry.GuardId;
                    continue;
                }

                if(entry.Action == GuardAction.FallsAsleep)
                {
                    asleepMinute = (short)entry.Timestamp.Minute;
                    continue;
                }

                if(entry.Action == GuardAction.WakesUp)
                {
                    short minute = (short)entry.Timestamp.Minute;

                    var mask = CalculateSleepMask(asleepMinute, (short)(minute - 1));

                    var dayIndex = entry.Timestamp.DayOfYear - dayMin;
                    sleepMasks[dayIndex] |= mask;
                    guardOnDuties[dayIndex] = guardOnDuty;
                    timeAsleep[dayIndex] += (short)(minute - asleepMinute);
                }
            }

            Span<DayData> dayData = new DayData[daysCount];

            for(int i = 0; i < daysCount; i++)
            {
                var sleepMask = sleepMasks[i];
                var guardId = guardOnDuties[i];
                var sleepTime = timeAsleep[i];
                dayData[i] = new DayData(sleepMask, guardId, sleepTime);
            }

            dayData.SortBy(x => x.GuardId);

            var guardMin = dayData[0].GuardId;
            var guardMax = dayData[daysCount - 1].GuardId;
            var guardCount = guardMax - guardMin + 1;

            Span<short> guardTotals = new short[guardCount];
            Span<short> minuteTotals = new short[guardCount * 60];

            for(int i = 0; i < daysCount; i++)
            {
                var data = dayData[i];

                var guardIndex = data.GuardId - guardMin;
                guardTotals[guardIndex] += data.SleepTime;

                for(int j = 0; j < 60; j++)
                {
                    minuteTotals[guardIndex * 60 + j] += (short)((data.SleepMask >> j) & 1);
                }
            }

            short guardTotalMax = 0;
            int guardIndexForMax = -1;
            for(int i = 0; i < guardCount; i++)
            {
                if(guardTotals[i] > guardTotalMax)
                {
                    guardTotalMax = guardTotals[i];
                    guardIndexForMax = i;
                }
            }

            Span<short> guardMinuteTotals = minuteTotals.Slice(guardIndexForMax * 60, 60);

            short minuteMax = 0;
            int minuteIndex = -1;

            for(int i = 0; i < 60; i++)
            {
                if(guardMinuteTotals[i] > minuteMax)
                {
                    minuteMax = guardMinuteTotals[i];
                    minuteIndex = i;
                }
            }

            return (guardIndexForMax + guardMin) * minuteIndex;
        }
        
        public int RunPartTwo(in Span<string> input)
        {
            var entries = this.ReadEntries(input);

            entries.SortBy(x => x.Timestamp);

            var dayMin = entries[0].Timestamp.DayOfYear;
            var dayMax = entries[entries.Length - 1].Timestamp.DayOfYear;
            var daysCount = dayMax - dayMin + 1;
            Span<ulong> sleepMasks = new ulong[daysCount];
            Span<short> guardOnDuties = new short[daysCount];
            Span<short> timeAsleep = new short[daysCount];

            short guardOnDuty = 0;
            short asleepMinute = 0;
            for(var i = 0; i < entries.Length; i++)
            {
                var entry = entries[i];
                if(entry.Action == GuardAction.Enters)
                {
                    guardOnDuty = entry.GuardId;
                    continue;
                }

                if(entry.Action == GuardAction.FallsAsleep)
                {
                    asleepMinute = (short)entry.Timestamp.Minute;
                    continue;
                }

                if(entry.Action == GuardAction.WakesUp)
                {
                    short minute = (short)entry.Timestamp.Minute;

                    var mask = CalculateSleepMask(asleepMinute, (short)(minute - 1));

                    var dayIndex = entry.Timestamp.DayOfYear - dayMin;
                    sleepMasks[dayIndex] |= mask;
                    guardOnDuties[dayIndex] = guardOnDuty;
                    timeAsleep[dayIndex] += (short)(minute - asleepMinute);
                }
            }

            Span<DayData> dayData = new DayData[daysCount];

            for(int i = 0; i < daysCount; i++)
            {
                var sleepMask = sleepMasks[i];
                var guardId = guardOnDuties[i];
                var sleepTime = timeAsleep[i];
                dayData[i] = new DayData(sleepMask, guardId, sleepTime);
            }

            dayData.SortBy(x => x.GuardId);

            var guardMin = dayData[0].GuardId;
            var guardMax = dayData[dayData.Length - 1].GuardId;
            var guardCount = guardMax - guardMin + 1;

            Span<short> guardTotals = new short[guardCount];
            Span<short> minuteTotals = new short[guardCount * 60];

            for(int i = 0; i < dayData.Length; i++)
            {
                var data = dayData[i];

                var guardIndex = data.GuardId - guardMin;
                guardTotals[guardIndex] += data.SleepTime;

                for(int j = 0; j < 60; j++)
                {
                    minuteTotals[guardIndex * 60 + j] += (short)((data.SleepMask >> j) & 1);
                }
            }

            short minuteMax = 0;
            int minuteIndex = -1;

            for(int i = 0; i < minuteTotals.Length; i++)
            {
                if(minuteTotals[i] > minuteMax)
                {
                    minuteMax = minuteTotals[i];
                    minuteIndex = i;
                }
            }

            var guardIndexForMax = minuteIndex / 60;

            return (guardIndexForMax + guardMin) * (minuteIndex % 60);
        }

        private ulong CalculateSleepMask(in short start, in short end)
        {
            var rightShift = ulongSize - end - 1;
            var rightMask = allOnes >> rightShift;

            var leftShift = start;
            var leftMask = allOnes << leftShift;

            return leftMask & rightMask;
        }

        private Span<EntryData> ReadEntries(in Span<string> input)
        {
            Span<EntryData> entries = new EntryData[input.Length];

            for(var i = 0; i < input.Length; i++)
            {
                entries[i] = this.ReadEntry(input[i]);
            }

            return entries;
        }

        private EntryData ReadEntry(string input)
        {
            var match = this.regex.Match(input);

            var year = short.Parse(match.Groups["Year"].Value);
            var month = byte.Parse(match.Groups["Month"].Value);
            var day = byte.Parse(match.Groups["Day"].Value);
            var hour = byte.Parse(match.Groups["Hour"].Value);
            var minute = byte.Parse(match.Groups["Minute"].Value);

            var guardAction = GuardAction.Invalid;
            short guardId = 0;

            if(match.Groups["GuardEnters"].Success)
            {
                guardAction = GuardAction.Enters;
                guardId = short.Parse(match.Groups["GuardID"].Value);
            }
            else if(match.Groups["FallsAsleep"].Success)
            {
                guardAction = GuardAction.FallsAsleep;
            }
            else if(match.Groups["WakesUp"].Success)
            {
                guardAction = GuardAction.WakesUp;
            }

            if(guardAction == GuardAction.Invalid)
            {
                throw new Exception("Guard action was incorrectly bound");
            }

            return new EntryData(year, month, day, hour, minute, guardAction, guardId);
        }

        private readonly struct EntryData
        {
            public readonly DateTime Timestamp;
            public readonly GuardAction Action;
            public readonly short GuardId;

            public EntryData(short year, byte month, byte day, byte hour, byte minute, GuardAction action, short guardId)
            {
                this.Timestamp = new DateTime(year, month, day, hour, minute, 0);
                this.Action = action;
                this.GuardId = guardId;
            }

            public static int Compare(EntryData a, EntryData b)
            {
                return a.Timestamp.CompareTo(b.Timestamp);
            }

            public override string ToString()
            {
                return this.Timestamp.ToString(CultureInfo.InvariantCulture);
            }
        }

        private readonly struct DayData
        {
            public readonly ulong SleepMask;
            public readonly short GuardId;
            public readonly short SleepTime;

            public DayData(ulong sleepMask, short guardId, short sleepTime)
            {
                this.SleepMask = sleepMask;
                this.GuardId = guardId;
                this.SleepTime = sleepTime;
            }
        }

        private enum GuardAction
        {
            Invalid,
            Enters,
            FallsAsleep,
            WakesUp
        }
    }
}
