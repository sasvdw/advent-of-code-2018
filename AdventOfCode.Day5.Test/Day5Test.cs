using System;
using Common;
using Xunit;

namespace AdventOfCode.Day5.Test
{
    public class Day5Test : TestRunnerBase<char, int, int>
    {
        private readonly char[] input = "dabAcCaCBAcCcaDA".ToCharArray();
        
        public Day5Test() : base(new SolutionDay5()) { }

        protected override char[] InputOne => this.input;
        protected override char[] InputTwo => this.input;
        protected override int ExpectedOne => 10;
        protected override int ExpectedTwo => 4;
    }
}
