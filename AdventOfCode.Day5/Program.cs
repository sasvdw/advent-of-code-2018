﻿using System;
using System.Text;
using static Input.FileInputReader;

namespace AdventOfCode.Day5
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputStrings = ReadInputFile("input.txt");

            var sb = new StringBuilder();

            for(int i = 0; i < inputStrings.Length; i++)
            {
                sb.Append(inputStrings[i]);
            }

            var input = sb.ToString().ToCharArray();
        
            var solution = new SolutionDay5();

            Console.WriteLine($"Part 1: {solution.RunPartOne(input)}");
            Console.WriteLine($"Part 2: {solution.RunPartTwo(input)}");
        }
    }
}
