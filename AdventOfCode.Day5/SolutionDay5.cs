using System;
using AdventOfCode.Common;
using static System.Math;

namespace AdventOfCode.Day5
{
    public class SolutionDay5 : ISolution<char, int, int>
    {
        private const string allElements = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private readonly string[] pairs;

        public SolutionDay5()
        {
            Span<char> elements = allElements.ToCharArray();
            this.pairs = new string[52];
            
            for(int i = 0; i < 26; i++)
            {
                var positiveElement = elements[i];
                var negativeElement = elements[i + 26];
                this.pairs[i] = $"{positiveElement}{negativeElement}";
                this.pairs[i + 26] = $"{negativeElement}{positiveElement}";
            }
        }
        
        public int RunPartOne(in Span<char> input)
        {
            var inputString = new string(input);

            return this.ReactPolymer(inputString);
        }

        private int ReactPolymer(string inputString)
        {
            var matchFound = true;

            while(matchFound)
            {
                matchFound = false;

                for(int i = 0; i < this.pairs.Length; i++)
                {
                    var newString = inputString.Replace(this.pairs[i], string.Empty);
                    matchFound |= inputString.Length - newString.Length > 0;
                    inputString = newString;
                }
            }

            return inputString.Length;
        }

        public int RunPartTwo(in Span<char> input)
        {
            var originalInput = new string(input);
            Span<char> elements = allElements.ToCharArray();

            var result = int.MaxValue;

            for(int i = 0; i < 26; i++)
            {
                var positiveElement = elements[i];

                var newInput = originalInput.Replace($"{positiveElement}", string.Empty, StringComparison.InvariantCultureIgnoreCase);

                var reactionResult = this.ReactPolymer(newInput);

                result = Min(result, reactionResult);
            }

            return result;
        }
    }
}
