using System;
using Common;
using Xunit;

namespace AdventOfCode.Day7.Test
{
    public class Day7Test : TestRunnerBase<string, string, int>
    {
        private readonly string[] input =
            new[]
            {
                "Step C must be finished before step A can begin.",
                "Step C must be finished before step F can begin.",
                "Step A must be finished before step B can begin.",
                "Step A must be finished before step D can begin.",
                "Step B must be finished before step E can begin.",
                "Step D must be finished before step E can begin.",
                "Step F must be finished before step E can begin."
            };
        
        public Day7Test() : base(new SolutionDay7(2, 0)) { }

        protected override string[] InputOne => this.input;
        protected override string[] InputTwo => this.input;
        protected override string ExpectedOne => "CABDFE";
        protected override int ExpectedTwo => 15;
    }
}
