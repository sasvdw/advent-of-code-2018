using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using AdventOfCode.Common;
using static System.Math;

namespace AdventOfCode.Day7
{
    public class SolutionDay7 : ISolution<string, string, int>
    {
        private const char offset = 'A';
        private readonly Regex regex = new Regex(@"Step (?'Start'[A-Z]) must be finished before step (?'End'[A-Z]) can begin\.", RegexOptions.Compiled);
        private readonly byte workerCount;
        private readonly byte workBaseTime;

        public SolutionDay7(byte workerCount, byte workBaseTime)
        {
            this.workerCount = workerCount;
            this.workBaseTime = workBaseTime;
        }
        
        public string RunPartOne(in Span<string> input)
        {
            var edges = this.ExtractEdges(input);

            uint dependentMask = 0;
            uint presentMask = 0;

            for(int i = 0; i < edges.Length; i++)
            {
                var startIndex = edges[i].Start - offset;
                var endIndex = edges[i].End - offset;
                var endMask = (uint)(1 << endIndex);

                presentMask |= (uint)(1 << startIndex);
                presentMask |= endMask;
                dependentMask |= endMask;
            }


            var presentComplimentMask = ~presentMask;
            dependentMask |= presentComplimentMask;
            var stepCount = this.CountTrailingZero(presentComplimentMask);
            Span<char> chars = new char[stepCount];
            Span<uint> dependencies = new uint[stepCount];

            for(int i = 0; i < edges.Length; i++)
            {
                var startIndex = edges[i].Start - offset;
                var endIndex = edges[i].End - offset;

                dependencies[endIndex] |= (uint)(1 << startIndex);
            }

            var finishedMask = presentComplimentMask;
            var processQueue = ~dependentMask;
            for(int i = 0; i < stepCount; i++)
            {
                var charIndex = CountTrailingZero(processQueue);

                char charValue = (char)(offset + charIndex);
                chars[i] = charValue;
                var valueMask = (uint)(1 << charIndex);
                finishedMask |= valueMask;
                var removeValueMask = ~valueMask;
                dependentMask &= removeValueMask;
                processQueue &= removeValueMask;

                for(int j = 0; j < dependencies.Length; j++)
                {
                    uint endMask = (uint)(1 << j);
                    endMask &= ~(endMask & finishedMask);
                    dependencies[j] &= removeValueMask;

                    if(dependencies[j] == 0 && endMask > 0)
                    {
                        processQueue |= endMask;
                    }
                }
            }

            return new string(chars);
        }

        public int RunPartTwo(in Span<string> input)
        {
            var edges = this.ExtractEdges(input);

            uint presentMask = 0;

            for(int i = 0; i < edges.Length; i++)
            {
                var startIndex = edges[i].Start - offset;
                var endIndex = edges[i].End - offset;
                var startMask = (uint)(1 << startIndex);
                var endMask = (uint)(1 << endIndex);

                presentMask |= startMask;
                presentMask |= endMask;
            }


            var stepCount = this.CountTrailingZero(~presentMask);
            Span<uint> dependencies = new uint[stepCount];

            for(int i = 0; i < edges.Length; i++)
            {
                var startIndex = edges[i].Start - offset;
                var endIndex = edges[i].End - offset;

                dependencies[endIndex] |= (uint)(1 << startIndex);
            }




            Span<WorkerItem> workers = new WorkerItem[this.workerCount];

            var timeElapsed = -1;
            var finishedWork = ~presentMask;
            uint queueMask = 0;
            uint selectedMask = 0;

            const byte one = 1;
                
            do
            {
                timeElapsed++;

                for(int i = 0; i < workers.Length; i++)
                {
                    var item = workers[i];

                    if(item.Mask > 0 && item.TimeLeft == 0)
                    {
                        var itemComplimentMask = ~item.Mask;

                        finishedWork |= item.Mask;
                        queueMask &= itemComplimentMask;
                        selectedMask &= itemComplimentMask;

                        item.Mask = 0;
                    }

                    workers[i] = item;
                }

                for(int i = 0; i < dependencies.Length; i++)
                {
                    dependencies[i] &= ~finishedWork;
                }

                var availableMask = ~(finishedWork | queueMask | selectedMask);

                for(int i = 0; i < dependencies.Length; i++)
                {
                    var itemMask = (uint)(1 << i);
                    itemMask &= availableMask;

                    if(dependencies[i] == 0)
                    {
                        queueMask |= itemMask;
                    }
                }

                for(int i = 0; i < workers.Length; i++)
                {
                    var item = workers[i];

                    if(item.Mask == 0 && queueMask > 0)
                    {
                        var itemToSelect = this.CountTrailingZero(queueMask);

                        var timeCost = this.workBaseTime + itemToSelect + 1;

                        item.Mask = (uint)(1 << itemToSelect);
                        selectedMask |= item.Mask;
                        queueMask &= ~item.Mask;
                        item.TimeLeft = timeCost;
                    }

                    item.TimeLeft = Max(item.TimeLeft, one);

                    item.TimeLeft--;
                    workers[i] = item;
                }
            }
            while((queueMask | selectedMask) > 0);
                
            return timeElapsed;
        }

        private Span<Edge> ExtractEdges(in Span<string> input)
        {
            Span<Edge> edges = new Edge[input.Length];

            for(int i = 0; i < input.Length; i++)
            {
                var match = this.regex.Match(input[i]);

                var start = match.Groups["Start"].Value.ToCharArray()[0];
                var end = match.Groups["End"].Value.ToCharArray()[0];

                edges[i] = new Edge(start, end);
            }

            return edges;
        }

        private int CountTrailingZero(in uint input)
        {
            Span<int> mod37BitPosition = // map a bit value mod 37 to its position
                new[]
                {
                    32, 0, 1, 26, 2, 23, 27, 0, 3, 16, 24, 30, 28, 11, 0, 13, 4,
                    7, 17, 0, 25, 22, 31, 15, 29, 10, 12, 6, 0, 21, 14, 9, 5,
                    20, 8, 19, 18
                };
            return mod37BitPosition[(int)((-input & input) % 37)];
        }
        
        private readonly struct Edge
        {
            public readonly char Start;
            public readonly char End;

            public Edge(char start, char end)
            {
                this.Start = start;
                this.End = end;
            }
        }

        [DebuggerDisplay("Item = {Mask}, Time = {TimeLeft}")]
        private struct WorkerItem
        {
            public uint Mask;
            public int TimeLeft;
        }
    }
}
