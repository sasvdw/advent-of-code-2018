using System;
using Common;
using Xunit;

namespace AdventOfCode.Day8.Test
{
    public class Day8Test : TestRunnerBase<int, int, int>
    {
        private readonly int[] input =
        {
            2, 3, 0, 3, 10, 11, 12, 1, 1, 0, 1, 99, 2, 1, 1, 2
        };

        public Day8Test() : base(new SolutionDay8()) { }

        protected override int[] InputOne => this.input;
        protected override int[] InputTwo => this.input;
        protected override int ExpectedOne => 138;
        protected override int ExpectedTwo => 66;
    }
}
