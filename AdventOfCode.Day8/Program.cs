﻿using System;
using static Input.FileInputReader;

namespace AdventOfCode.Day8
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputFile = ReadInputFile("input.txt", ' ');

            Span<int> input = new int[inputFile.Length];

            for(int i = 0; i < inputFile.Length; i++)
            {
                input[i] = int.Parse(inputFile[i]);
            }

            var solution = new SolutionDay8();

            Console.WriteLine($"Part 1: {solution.RunPartOne(input)}");
            Console.WriteLine($"Part 2: {solution.RunPartTwo(input)}");
        }
    }
}
