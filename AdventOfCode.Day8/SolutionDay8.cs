﻿using System;
using AdventOfCode.Common;

namespace AdventOfCode.Day8
{
    public class SolutionDay8 : ISolution<int, int, int>
    {
        public int RunPartOne(in Span<int> input)
        {
            var total = 0;

            var rootNode = Node.LoadRootNode(input);
            total += rootNode.CalculateAllMetaDataTotal();

            return total;
        }

        public int RunPartTwo(in Span<int> input)
        {
            var total = 0;

            var rootNode = Node.LoadRootNode(input);
            total += rootNode.CalculateValueUsingMetaData();

            return total;
        }
        
        private class Node
        {
            private readonly int startIndex;
            private readonly Node[] children;
            private readonly int[] metaData;

            private int metaDataTotal;
            private int size;
            
            private Node(int startIndex, int childCount, int metaDataCount)
            {
                this.startIndex = startIndex;
                this.children = new Node[childCount];
                this.metaData = new int[metaDataCount];
            }

            public int CalculateAllMetaDataTotal()
            {
                var total = this.metaDataTotal;

                foreach(var child in this.children)
                {
                    total += child.CalculateAllMetaDataTotal();
                }

                return total;
            }

            public int CalculateValueUsingMetaData()
            {
                if(this.children.Length == 0)
                {
                    return this.metaDataTotal;
                }

                var value = 0;

                foreach(var meta in this.metaData)
                {
                    var index = meta - 1;
                    if(index >= this.children.Length)
                    {
                        continue;
                    }

                    if(index < 0)
                    {
                        continue;
                    }

                    value += this.children[index].CalculateValueUsingMetaData();
                }

                return value;
            }

            private void AddChildNodesAndMetaData(in Span<int> input)
            {
                if(this.children.Length == 0)
                {
                    var metadataOffset = this.startIndex + 2;
                    
                    this.LoadMetadata(input, metadataOffset);

                    this.size = 2 + this.metaData.Length;
                    return;
                }
                
                var baseOffset = this.startIndex + 2;
                var childrenSize = 0;
                for(int i = 0; i < this.children.Length; i++)
                {
                    var childStartIndex = baseOffset + childrenSize;
                    var children = input[childStartIndex];
                    var metadata = input[childStartIndex + 1];
                    var child = new Node(childStartIndex, children, metadata);

                    child.AddChildNodesAndMetaData(input);
                    childrenSize += child.size;

                    this.children[i] = child;
                }

                this.LoadMetadata(input, baseOffset + childrenSize);

                this.size = 2 + childrenSize + this.metaData.Length;
            }

            private void LoadMetadata(Span<int> input, int metadataOffset)
            {
                for(int i = 0; i < this.metaData.Length; i++)
                {
                    var metaDataValue = input[metadataOffset + i];
                    this.metaData[i] = metaDataValue;
                    this.metaDataTotal += metaDataValue;
                }
            }

            public static Node LoadRootNode(in Span<int> input)
            {
                var children = input[0];
                var metadata = input[1];

                var node = new Node(0, children, metadata);

                node.AddChildNodesAndMetaData(input);

                return node;
            }
        }
    }
}
