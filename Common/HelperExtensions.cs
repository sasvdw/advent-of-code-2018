using System;

namespace Common
{
    public static class HelperExtensions
    {
        public static Span<T> CopyToNewSpan<T>(this T[] arr)
        {
            var newArr = new T[arr.Length];
            Span<T> span = newArr;
            arr.CopyTo(span);
            return span;
        }
    }
}
