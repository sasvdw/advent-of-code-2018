using AdventOfCode.Common;
using Xunit;

namespace Common
{
    public abstract class TestRunnerBase<TInput, TOutputOne, TOutputTwo>
    {
        protected abstract TInput[] InputOne { get; }
        
        protected abstract TInput[] InputTwo { get; }
        
        protected abstract TOutputOne ExpectedOne { get; }

        protected abstract TOutputTwo ExpectedTwo { get; }
        
        private readonly ISolution<TInput, TOutputOne, TOutputTwo> solution;

        protected TestRunnerBase(ISolution<TInput, TOutputOne, TOutputTwo> solution)
        {
            this.solution = solution;
        }
        
        [Fact]
        public void TestPartOne()
        {
            var actual = this.solution.RunPartOne(this.InputOne.CopyToNewSpan());

            Assert.Equal(this.ExpectedOne, actual);
        }

        [Fact]
        public void TestPartTwo()
        {
            var actual = this.solution.RunPartTwo(this.InputTwo.CopyToNewSpan());

            Assert.Equal(this.ExpectedTwo, actual);
        }
    }
}
