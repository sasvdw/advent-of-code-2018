﻿using System;
using System.Collections.Generic;
using AdventOfCode.Common;

namespace Day2
{
    public class SolutionDay2 : ISolution<string, int, string>
    {
        public int RunPartOne(in Span<string> input)
        {
            var twoCount = 0;
            var threeCount = 0;

            for(int i = 0; i < input.Length; i++)
            {
                var itemToProcess = input[i];

                var hasTwoCount = false;
                var hasThreeCount = false;
                while(itemToProcess.Length > 0)
                {
                    var first = itemToProcess[0];

                    var removed = itemToProcess.Replace(first.ToString(), string.Empty);

                    var count = itemToProcess.Length - removed.Length;
                    hasTwoCount |= count == 2;
                    hasThreeCount |= count == 3;

                    itemToProcess = removed;
                }

                if(hasTwoCount)
                {
                    twoCount++;
                }

                if(hasThreeCount)
                {
                    threeCount++;
                }
            }

            return twoCount * threeCount;
        }

        public string RunPartTwo(in Span<string> input)
        {
            for(int i = 0; i < input.Length; i++)
            {
                for(int j = i + 1; j < input.Length; j++)
                {
                    var one = input[i].ToCharArray();
                    var two = input[j].ToCharArray();

                    var differences = new Stack<int>();
                    for(int k = 0; k < one.Length; k++)
                    {
                        if(one[k] == two[k])
                        {
                            continue;
                        }

                        differences.Push(k);
                    }

                    if(differences.Count == 1)
                    {
                        var index = differences.Pop();
                        return input[i].Remove(index, 1);
                    }
                }
            }

            return string.Empty;
        }
    }
}
