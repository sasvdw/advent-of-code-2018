using System.IO;

namespace Input
{
    public static class FileInputReader
    {
        public static string[] ReadInputFile(string fileName, char split = '\n')
        {
            using(var st = new FileStream(fileName, FileMode.Open))
            using(var sr = new StreamReader(st))
            {
                var file = sr.ReadToEnd();

                return file.Trim().Split(split);
            }
        }
    }
}
